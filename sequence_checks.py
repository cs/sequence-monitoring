#!/usr/bin/env python3

import asyncio
import pathlib
import time
import asyncio
import os
from qcontrol3.client.http import AsyncTimingClient
from asyncio import subprocess
from config import *
import config
import time
import async_timeout
from async_timeout import *
import urllib.request
import logging
from PIL import Image
import os
import numpy as np
import lmfit
import matplotlib.pyplot as plt
import h5py
import json
from astropy import units as u

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt

from lmfit.models import LorentzianModel, GaussianModel, ConstantModel, LinearModel

from copy import copy
from scipy.optimize import curve_fit
from cs_analysis.data_handling import load_run, load_images, load_iter, load_all_param
from cs_analysis.plot_utils import show_image, colors, plot_lattice_image
from cs_analysis.reconstruction import preview_lattice_span, span_lattice, get_lattice_mask
from cs_analysis.evaluation import correct_phase_jumps
from lmfit import Model, Parameter, report_fit
from matplotlib.patches import Polygon
plt.rcParams['figure.figsize'] = (8,5)
plt.rcParams['figure.dpi'] = 100
plt.rcParams['figure.facecolor'] = 'white'
    
plt.style.use('https://git.io/JLiCd')
import warnings
warnings.filterwarnings('ignore')

Gamma = 5.2 * u.MHz
Sigma0 = 8.26e-10 * u.cm ** 2 #assuming isotropic polarization, F=3->F=2
Isat = 4.63 * u.mW / u.cm ** 2

def fit_1d_gauss(name, data, axis=None, plot_result=False, guess=None):
    if axis is None:
        axis = np.arange(len(data))
    g = lmfit.models.GaussianModel()
    g.set_param_hint('amplitude', min=0)
    
    o = lmfit.models.ConstantModel()
    
    if guess is None:
        params = o.make_params(c=data[0])
        params += g.guess(data, x=axis)
    else:
        params = guess
    fit = (g + o).fit(data, params, x=axis)

    if plot_result:
        plt.figure(figsize=(10,6))
        plt.plot(axis, data, axis, fit.best_fit)
        plt.savefig(f"tmp/{name}.png")
    return fit

    
def dict_to_prometheus(fitting_results):
    output_string = ""
    timestamp = int(time.time()*1000)
    for zone in fitting_results:
        fit_success = fitting_results[zone]["success"]
        output_string += 'camera_monitor_fit_success{zone="' + zone + '"} ' +  f"{int(fit_success)} {timestamp}" + "\n"
        
        if fit_success:
            value = fitting_results[zone]["x_center"]
            output_string += 'camera_monitor_fit_pixel{zone="' + zone + '", axis="x", var="center"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results[zone]["y_center"]
            output_string += 'camera_monitor_fit_pixel{zone="' + zone + '", axis="y", var="center"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results[zone]["x_width"]
            output_string += 'camera_monitor_fit_pixel{zone="' + zone + '", axis="x", var="sigma"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results[zone]["y_width"]
            output_string += 'camera_monitor_fit_pixel{zone="' + zone + '", axis="y", var="sigma"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results[zone]["x_amp"]
            output_string += 'camera_monitor_fit_pixel{zone="' + zone + '", axis="x", var="amplitude"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results[zone]["y_amp"]
            output_string += 'camera_monitor_fit_pixel{zone="' + zone + '", axis="y", var="amplitude"} ' +  f"{value} {timestamp}" + "\n"
    return output_string


def get_qcontrol_image(full_path, cam_data_folder):
    img_path=cam_data_folder
    with h5py.File(full_path, "r") as f:
        img = np.asarray(f[f"/data/{img_path}/image/0"], dtype=float)
    
    return img

def get_qcontrol_absorption_image(full_path, cam_data_folder):
    img_path=cam_data_folder
    with h5py.File(full_path, "r") as f:
        img1 = np.asarray(f[f"/data/{img_path}/image/0"], dtype=float)
        img2 = np.asarray(f[f"/data/{img_path}/image/1"], dtype=float)
        ref = np.asarray(f[f"/data/{img_path}/image/2"], dtype=float)
        img2 -= ref
        img1 -= ref
        img1[img1 < 1] = 1
        img2[img2 < 1] = 1
        img = np.log(img2/img1)
    return img


def substitute_expression(filename_in, old, new, filename_out):
    with open(filename_in, 'r') as file :
        filedata = file.read()

    filedata = filedata.replace(old, new)

    with open(filename_out, 'w') as file:
        file.write(filedata)

def scatt_cross(I=Isat, Delta=0 * u.MHz, Gamma=Gamma, Isat=Isat, Sigma0=Sigma0):
    """ defaults: I=Isat, Delta=0 MHz"""
    return Sigma0 / (1 + I / Isat + 4 * Delta ** 2 / Gamma ** 2)


def get_recon_filename(file_path):
    is_idle_seq = "idle" in file_path

    path_components = file_path.split('/')
    
    if not is_idle_seq:
        date_part = '/'.join(path_components[:3])
        run_name = path_components[3]
        
        store_path = config.ana_base_path + '/' + date_part + '/processed_ae/'
    else:
        run_name = "idle_recon"
        store_path = config.ana_base_path + '/idle/processed_ae/'
    result_file_path = store_path + run_name + ".nc"
    return result_file_path



class BeamMonitoring:
    config = None
    
    def __init__(self, config):
        self.config = config
        
        
    
    async def during_sequence(self):
        fitting_results = dict()
        for camera in self.config.cameras:
            name = self.config.cameras[camera]["name"]
            url = self.config.cameras[camera]["url"]
            zones = self.config.cameras[camera]["zones"]
            if "http" in url:
                path = f"tmp/{name}.jpg"
                if os.path.exists(path):
                    os.remove(path)
                else:
                    logging.info(logging.info(f'Seems like you never took an image with {name}'))
                
                logging.info(f'Taking image with cam {name}')
                urllib.request.urlretrieve(url, path)
                time.sleep(1)
                    
            elif url == "qcontrol3":
                dummy = 0
            else:
                logging.error(f'Invalid url for cam {name}')
                
    async def prepare(self):
        filename_in = "scripts/take_qcontrol3_imgs.pys"
        filename_out = "sequence_out.pys"
        
        #Get qcontrol camewras from config
        cam_ids = []
        for camera in self.config.cameras:
            name = self.config.cameras[camera]["name"]
            url = self.config.cameras[camera]["url"]
            zones = self.config.cameras[camera]["zones"]
            if url == "qcontrol3":
                cam_id = self.config.cameras[camera]["cam_id"]
                cam_data_folder = self.config.cameras[camera]["cam_data_folder"]
                cam_ids.append(cam_id)
        
        
        #Build Sequence
        substitute_expression(filename_in, "__CAM_IDS__", f"{cam_ids}", filename_out)
        
        return filename_out
        
        
    async def evaluate(self, qcontrol_data_path):
        
        fitting_results = await self.analyze_images(self.config.data_root_path + qcontrol_data_path)
        metrics = dict_to_prometheus(fitting_results)
        return metrics

    async def analyze_images(self, qcontrol_data_path = None):
        fitting_results = dict()
        
        
        for camera in self.config.cameras:
            name = self.config.cameras[camera]["name"]
            url = self.config.cameras[camera]["url"]
            zones = self.config.cameras[camera]["zones"]
            if "http" in url:
                path = f"tmp/{name}.jpg"
                try:
                    im = Image.open(path)
                except Exception:
                    print(f"Error: Seems like taking pi image of {camera} did not work")
                    continue
                for zone in zones:
                    logging.info(f'Cropping zone {zone} of cam {camera}')
                    zone_path = f"tmp/zone_{zone}.jpg"
                    im_cropped = im.crop((zones[zone]["startx"], zones[zone]["starty"], zones[zone]["endx"], zones[zone]["endy"]))
                    im_cropped.save(zone_path)
                    
                    logging.info(f'Fitting zone {zone} of cam {camera}')
                    im_data = np.sum(np.array(im_cropped)[:,:,:], axis=2)
                    x_fit = fit_1d_gauss(f"zone_{zone}_xfit", np.sum(im_data, axis=0), axis=None, plot_result=True, guess=None)
                    y_fit = fit_1d_gauss(f"zone_{zone}_yfit", np.sum(im_data, axis=1), axis=None, plot_result=True, guess=None)
                    
                    x_width, x_center,  x_amp, y_width, y_center, y_amp = x_fit.params["sigma"].value, x_fit.params["center"].value, x_fit.params["amplitude"].value, y_fit.params["sigma"].value, y_fit.params["center"].value, y_fit.params["amplitude"].value
                    
                    logging.info(f'Fitting results for zone {zone} of cam {camera}: sigmax={x_width}, centerx={x_center}, ampx={x_amp}, sigmay={y_width}, centery={y_center}, ampy={y_amp}')
                    
                    fit_success = int((x_width < zones[zone]["max_sigma"]) and (y_width < zones[zone]["max_sigma"]) and (x_amp> zones[zone]["min_ampl"]) and (y_amp> zones[zone]["min_ampl"]))
                    
                    zone_fitting_results = dict(x_center = x_center, x_width = x_width, x_amp = x_amp, y_center = y_center, y_width = y_width,  y_amp = y_amp, success=fit_success)
                    
                    fitting_results[zone] = zone_fitting_results 
            elif url == "qcontrol3":
                
                cam_id = self.config.cameras[camera]["cam_id"] 
                cam_data_folder = self.config.cameras[camera]["cam_data_folder"] 
                
                
                print("IMAGE PATH", qcontrol_data_path, cam_data_folder)
                im = np.asarray(get_qcontrol_image(qcontrol_data_path, cam_data_folder))
                for zone in zones:
                    logging.info(f'Cropping zone {zone} of cam {camera}')
                    zone_path = f"tmp/zone_{zone}.jpg"
                    im_cropped = im[zones[zone]["starty"]:zones[zone]["endy"], zones[zone]["startx"]:zones[zone]["endx"]]
                    plt.imsave(zone_path, im_cropped, cmap='Greys')
                        
                    logging.info(f'Fitting zone {zone} of cam {camera}')
                    im_data = im_cropped
                    x_fit = fit_1d_gauss(f"zone_{zone}_xfit", np.sum(im_data, axis=0), axis=None, plot_result=True, guess=None)
                    y_fit = fit_1d_gauss(f"zone_{zone}_yfit", np.sum(im_data, axis=1), axis=None, plot_result=True, guess=None)
                        
                    x_width, x_center,  x_amp, y_width, y_center, y_amp = x_fit.params["sigma"].value, x_fit.params["center"].value, x_fit.params["amplitude"].value, y_fit.params["sigma"].value, y_fit.params["center"].value, y_fit.params["amplitude"].value
                        
                    logging.info(f'Fitting results for zone {zone} of cam {camera}: sigmax={x_width}, centerx={x_center}, ampx={x_amp}, sigmay={y_width}, centery={y_center}, ampy={y_amp}')
                    fit_success = int((x_width < zones[zone]["max_sigma"]) and (y_width < zones[zone]["max_sigma"]) and ( x_amp > zones[zone]["min_ampl"]) and (y_amp> zones[zone]["min_ampl"]))
                    zone_fitting_results = dict(x_center = x_center, x_width = x_width, x_amp = x_amp, y_center = y_center, y_width = y_width,  y_amp = y_amp, success=fit_success)
                    fitting_results[zone] = zone_fitting_results 
            else:
                logging.error(f'Invalid url for cam {name}')
            print(json.dumps(fitting_results, indent=4, sort_keys=True))
        return fitting_results   


class TransportMonitoring:
    config = None
    
    def __init__(self, config):
        self.config = config

    async def prepare(self):
        filename_in = "scripts/transport.pys"
        filename_out = "sequence_out.pys"
        substitute_expression(filename_in, "!§%&/()=", f"----", filename_out)
        
        return filename_out

    async def during_sequence(self):
        # code to be run once it is detected that then shot has started
        return 0
                
        
    async def evaluate(self, qcontrol_data_path):
        
        cam_id = "manta" 
        cam_data_folder = "gc_y"

        im = np.asarray(get_qcontrol_absorption_image(self.config.data_root_path + qcontrol_data_path, cam_data_folder))
        path = f"tmp/transport.jpg"
        plt.imsave(path, im, cmap='Greys')
        im_data = im
        x_fit = fit_1d_gauss(f"xfit", np.sum(im_data, axis=0), axis=None, plot_result=True, guess=None)
        y_fit = fit_1d_gauss(f"yfit", np.sum(im_data, axis=1), axis=None, plot_result=True, guess=None)
            
        x_width, x_center,  x_amp, y_width, y_center, y_amp = x_fit.params["sigma"].value, x_fit.params["center"].value, x_fit.params["amplitude"].value, y_fit.params["sigma"].value, y_fit.params["center"].value, y_fit.params["amplitude"].value
            
        logging.info(f'Fitting results for transport img: sigmax={x_width}, centerx={x_center}, ampx={x_amp}, sigmay={y_width}, centery={y_center}, ampy={y_amp}')

        pixel_size = 2.36*u.um
        scatt = scatt_cross(I=0.3*u.mW/u.cm**2, 
              Delta=0*u.MHz, 
              Isat=46.3*u.kg/u.s**3, 
              Sigma0=8.26e-14*u.m**2, 
              Gamma=5.2227*u.MHz)


        fitting_results = dict(x_center = x_center, x_width = x_width, x_N = (x_amp/scatt*pixel_size**2).si, y_center = y_center, y_width = y_width,  y_N = (y_amp/scatt*pixel_size**2).si, success=True)

        output_string = ""
        timestamp = int(time.time()*1000)
        
        fit_success = fitting_results["success"]
        metrics_id = "after_transport_atoms_monitor"
        output_string += metrics_id + '_fit_success ' +  f"{int(fit_success)} {timestamp}" + "\n"
        if fit_success:
            value = fitting_results["x_center"]
            output_string += metrics_id + '_fit{axis="x", var="center"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["y_center"]
            output_string += metrics_id + '_fit{axis="y", var="center"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["x_width"]
            output_string += metrics_id + '_fit{axis="x", var="sigma"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["y_width"]
            output_string += metrics_id + '_fit{axis="y", var="sigma"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["x_N"]
            output_string += metrics_id + '_fit{axis="x", var="atom_number"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["y_N"]
            output_string += metrics_id + '_fit{axis="y", var="atom_number"} ' +  f"{value} {timestamp}" + "\n"
        return output_string


class BECMonitoring:
    config = None
    
    def __init__(self, config):
        self.config = config

    async def prepare(self):
        filename_in = "scripts/bec.pys"
        filename_out = "sequence_out.pys"
        substitute_expression(filename_in, "!§%&/()=", f"----", filename_out)
        
        return filename_out

    async def during_sequence(self):
        # code to be run once it is detected that then shot has started
        return 0
                
        
    async def evaluate(self, qcontrol_data_path):
        
        cam_id = "manta" 
        cam_data_folder = "gc_y"

        im = np.asarray(get_qcontrol_absorption_image(self.config.data_root_path + qcontrol_data_path, cam_data_folder))[300:640,:540]
        path = f"tmp/bec.jpg"
        plt.imsave(path, im, cmap='Greys')
        im_data = im
        x_fit = fit_1d_gauss(f"xfit", np.sum(im_data, axis=0), axis=None, plot_result=True, guess=None)
        y_fit = fit_1d_gauss(f"yfit", np.sum(im_data, axis=1), axis=None, plot_result=True, guess=None)
            
        x_width, x_center,  x_amp, y_width, y_center, y_amp = x_fit.params["sigma"].value, x_fit.params["center"].value, x_fit.params["amplitude"].value, y_fit.params["sigma"].value, y_fit.params["center"].value, y_fit.params["amplitude"].value
            
        logging.info(f'Fitting results for bec img: sigmax={x_width}, centerx={x_center}, ampx={x_amp}, sigmay={y_width}, centery={y_center}, ampy={y_amp}')

        pixel_size = 2.36*u.um
        scatt = scatt_cross(I=0.3*u.mW/u.cm**2, 
              Delta=0*u.MHz, 
              Isat=46.3*u.kg/u.s**3, 
              Sigma0=8.26e-14*u.m**2, 
              Gamma=5.2227*u.MHz)


        fitting_results = dict(x_center = x_center, x_width = x_width, x_N = (x_amp/scatt*pixel_size**2).si, y_center = y_center, y_width = y_width,  y_N = (y_amp/scatt*pixel_size**2).si, success=True)

        output_string = ""
        timestamp = int(time.time()*1000)
        
        fit_success = fitting_results["success"]
        metrics_id = "bec_atoms_monitor"
        output_string += metrics_id + '_fit_success ' +  f"{int(fit_success)} {timestamp}" + "\n"
        if fit_success:
            value = fitting_results["x_center"]
            output_string += metrics_id + '_fit{axis="x", var="center"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["y_center"]
            output_string += metrics_id + '_fit{axis="y", var="center"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["x_width"]
            output_string += metrics_id + '_fit{axis="x", var="sigma"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["y_width"]
            output_string += metrics_id + '_fit{axis="y", var="sigma"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["x_N"]
            output_string += metrics_id + '_fit{axis="x", var="atom_number"} ' +  f"{value} {timestamp}" + "\n"
            value = fitting_results["y_N"]
            output_string += metrics_id + '_fit{axis="y", var="atom_number"} ' +  f"{value} {timestamp}" + "\n"
        return output_string



class SuperlatticePhaseMonitoring:

    def __init__(self, config):
        self.config = config
        print(config)

    async def prepare(self):
        filename_in = self.config["script_name"]
        filename_out = "sequence_out.pys"

        substitute_expression(filename_in, "__RUNID__", f"{time.time()}", filename_out)
        
        return filename_out

    async def during_sequence(self):
        # code to be run once it is detected that then shot has started
        return 0
                
        
    async def evaluate(self, qcontrol_data_path):
        print(qcontrol_data_path)
        ds = xr.load_dataset(get_recon_filename(qcontrol_data_path), engine="h5netcdf").dropna("it").isel(i_im=0)
        ds= correct_phase_jumps(ds)
        ds["it"]=ds["it0"]
        ds= ds.drop_duplicates("it")


        help_plots=False
        ######### Plot Occupations ###########
        if help_plots:
            ds.occupations.isel(it=slice(0, 30)).plot(col='it', col_wrap=5, subplot_kws=dict(aspect="equal"), add_colorbar=False)
            plt.show()

        all_occs = copy(ds.occupations.data)


        ############# FIT AND PHASE CALIB PLOT #################
        plt.figure(figsize=(10,6))

        plt.subplot(121)

        even_occs = []
        odd_occs = []
        even_occs_loc = []
        odd_occs_loc = []
        deltas = []
        its = []
        for i in range(len(ds.it)):
            if np.sum(all_occs[i]) != 10000 and np.sum(np.isnan(all_occs[i]))==0:
                even_rows = all_occs[i,:,:].sum(self.config["axis"])[::2]
                odd_rows = all_occs[i,:,:].sum(self.config["axis"])[1::2]
                even_occs.append(even_rows.sum()/(even_rows.sum()+odd_rows.sum()))
                odd_occs.append(odd_rows.sum()/(even_rows.sum()+odd_rows.sum()))
                even_occs_loc.append(even_rows/(even_rows.sum()+odd_rows.sum()))
                odd_occs_loc.append(odd_rows/(even_rows.sum()+odd_rows.sum()))
                deltas.append((even_rows.sum()-odd_rows.sum())/(even_rows.sum()+odd_rows.sum()))
                its.append(ds.it[i])

        deltas = np.asarray(deltas)
        its = np.asarray(its)

        plt.plot(np.asarray(its), deltas[:], ls="None",marker="o",mfc="red", mec="darkred", label="Data")
        
        def sigmoid(a,k,x,x0):
            return a*(1 / (1 + np.exp(-k*(x-x0))) - 0.5)

        model = Model(sigmoid, independent_vars=['x'])

        delays = its[~np.isnan(deltas)]
        ydata = deltas[~np.isnan(deltas)]
        print(delays, ydata)
        print(np.asarray(its), deltas[:])
        xdata = np.linspace(min(delays), max(delays), 500)


        #make a guess on the sign of the sigmoid fit based on the mean of the first and the last data points
        k_guessed = 1
        if np.mean(deltas[:3]) > np.mean(deltas[-3:]):
            k_guessed = -1
        result = model.fit(ydata, x=delays, a=1.8, k=k_guessed, x0=-0.006)

        print(result.fit_report())
        plt.plot(delays, result.best_fit, ls="dashed", color="black")
        plt.title(f"Symmetric point: ({result.params['x0'].value} + {result.params['x0'].stderr}) V") 
        plt.xlabel("Phase Voltage (V)")
        plt.axhline(0, color="grey")
        plt.ylabel("Imbalance")

        plt.legend()

        even_occs_loc = np.asarray(even_occs_loc)
        odd_occs_loc = np.asarray(odd_occs_loc)

        plt.subplot(122)
        natoms = all_occs.sum(axis=(1,2))
        plt.plot(ds.it, natoms,ls="None",marker="o",mfc="lightblue", mec="navy")
        plt.xlabel("Phase voltage (V)")
        plt.ylabel("Detected Atoms")
        plt.tight_layout()

        plt.savefig("tmp/sl"+str(self.config["axis"])+"_phasecalib.png")

        

        fit_success = True
        if (result.params['x0'].value < -0.01) or (result.params['x0'].value > 0.01) or ((result.params['x0'].stderr or 0) > 0.0015):
            fit_success = False
            print("ERROR: Lost Symmetry point.")



        #OUTPUT METRICS FOR GRAFANA
        output_string = ""
        timestamp = int(time.time()*1000)

        metrics_id = self.config["metrics_id"]
        output_string += metrics_id + '_fit_success ' +  f"{int(fit_success)} {timestamp}" + "\n"
        if fit_success:


            #Read former result
            file = open(self.config["sl_phasecalib_output_file"], "r")
            old_value = float(file.read())
            file.close()
            print("old value", old_value)

            # WRITE FIT RESULT TO FILE
            value = old_value + result.params['x0'].value
            file = open(self.config["sl_phasecalib_output_file"], "w")
            file.write(f"{value}")
            file.close()


            output_string += metrics_id + '_fit{var="center"} ' +  f"{value} {timestamp}" + "\n"
            output_string += metrics_id + '_fit{var="center_err"} ' +  f"{result.params['x0'].stderr} {timestamp}" + "\n"
            output_string += metrics_id + '_fit{var="amplitude"} ' +  f"{result.params['a'].value} {timestamp}" + "\n"
        return output_string


