from fastapi import FastAPI
from fastapi.responses import PlainTextResponse
import uvicorn
from config import *

app = FastAPI()

@app.get("/metrics", response_class=PlainTextResponse)
async def main():
    content1 = ""
    content2 = ""

    try:
        with open('sequence_metrics.txt', "r") as f:
            content1 = f.read()
        with open('ai_metrics.txt', "r") as f:
            content2 = f.read()
    except FileNotFoundError:
        print("No metrics files found yet. Is sequence / ai monitoring running?")



    all_content = content1 + "\n" + content2    
    print(all_content)
    return all_content



if __name__ == '__main__':
    uvicorn.run(app, port=server["port"], host=server["host"])
