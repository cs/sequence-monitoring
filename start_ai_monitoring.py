#!/usr/bin/env python3

import asyncio
import pathlib
import time
import os
from qcontrol3.client.http import AsyncTimingClient
from asyncio import subprocess
import config
import time
import async_timeout
from async_timeout import *
from ai_checks import *


async def run_sequence():
    
#Schedule sequence
    tc = AsyncTimingClient()
    async for topic, event in tc.events.events("task.run.end"):
        new_queue_id = event["queue_id"]
        data_file_name = event["data_file_name"]
        print(f"Evaluating analog inputs", new_queue_id )
        time.sleep(5)
        all_metrics = ""
        try:
            metrics = await evaluate_ai_checks(config.analog_input_checks, config.data_root_path +  data_file_name, data_file_name)
            all_metrics = all_metrics + "\n" + metrics
            print("Finished.")
                            
            print(all_metrics)
            
            #write metrics
            print("Write metrics...")
            with open("ai_metrics.txt","w") as file:
                file.write(all_metrics)
                file.close()
        except:
            print("some error")
            
    

if __name__ == "__main__":
    asyncio.run(run_sequence())
