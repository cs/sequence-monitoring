#!/usr/bin/env python3

import asyncio
import pathlib
import time
import os
from qcontrol3.client.http import AsyncTimingClient
from asyncio import subprocess
import config
import time
import async_timeout
from async_timeout import *


root = pathlib.Path("/mnt/data/qcontrol3")
#root = pathlib.Path("/home/jovyan/qcontrol3")
path = "sequence.pys"




    
def arm_sequence(in_name, ut_name):
    os.system(f'cp {in_name} {root / out_name}')



async def run_sequence():
    
#Schedule sequence

    all_metrics = ""
    for current_check in config.checks.values():
        
        handler_class = current_check["handler_class"]
        check_object = handler_class(current_check)
        sequence_file = await check_object.prepare()
        print("sequence file: ", sequence_file)
        with open(sequence_file, "r") as f:
            script = f.read()
        tc = AsyncTimingClient()
        os.system(f'cp {sequence_file} {root / path}')  
        handle = await tc.queue_script_iteration(path, priority="now", auto_start=True)
        
        
        queue_id_start = handle.children[0].queue_id
        queue_id_end = handle.children[-1].queue_id
        
        #Wait for the sequence's turn, then start pi data collection
        async with timeout(config.wait_for_shot_started_timeout):
            async for topic, event in tc.events.events("task.run.start"):
                new_queue_id = event["queue_id"]
                if queue_id_start == new_queue_id:
                    if current_check["do_sth_during_sequence"]: await check_object.during_sequence()
                    break
        
        # wait for run to be finished
        async with timeout(config.wait_for_shot_finished_timeout):
            async for topic, event in tc.events.events("task.run.end"):
                new_queue_id = event["queue_id"]
                data_file_name = event["data_file_name"]
                print(f"Run saved in hdf5 file {data_file_name}")
                if queue_id_end == new_queue_id:
                    time.sleep(5)
                    print("Run has finished.")
                    print("Start evaluation")
                    metrics = await check_object.evaluate(data_file_name)
                    all_metrics = all_metrics + "\n" + metrics
                    print("Finished.")
                    break
                    
                    


        print(all_metrics)
        
        #write metrics
        print("Write metrics...")
        with open("sequence_metrics.txt","w") as file:
            file.write(all_metrics)
            file.close()
    

if __name__ == "__main__":
    asyncio.run(run_sequence())
