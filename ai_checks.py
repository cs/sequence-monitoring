
import numpy as np
import os
import h5py
import xarray as xr
from config import *
import time
from scipy.optimize import leastsq

from sequence_checks import get_recon_filename

async def evaluate_ai_checks(analog_input_checks, file_path, file_path_short):
    import numpy as np
    result=dict()
    output_string = ""
    timestamp = int(time.time()*1000)
    
    
    ###########   HDF 5 File ##############
    with h5py.File(file_path) as f:
        for check_id in analog_input_checks:
            try:
                data_path =  analog_input_checks[check_id]["data_path"]
                start_path =  analog_input_checks[check_id]["start_path"]
                freq_path =  analog_input_checks[check_id]["freq_path"]
                check_function =  analog_input_checks[check_id]["check_function"]
                data = f[data_path][()]
                start = f[start_path][()]
                freq = f[freq_path][()]
                times = np.arange(len(data))/freq + start
                observable = check_function(data, times)
            except Exception as e:
                print(e)
                data = None
                times = None
                observable = -1
            
            data_stack = np.vstack((times, data)).T
            data_dict = []
            for data_pair in data_stack:
                data_dict.append(dict(x=data_pair[0], y=data_pair[1]))
            result[check_id] = dict()
            result[check_id]["data"] = data_dict
            result[check_id]["value"] = observable
            output_string += 'ai_monitoring{check="'+  check_id + '"} ' +  f"{observable} {timestamp}" + "\n"

        try:
            img = np.asarray(f[f"/data/kinetix/image/0"], dtype=float)
            check_id = "fluo_signal"
            fluo_measure = np.mean(img)
            output_string += 'sequence_monitoring{check="'+  check_id + '"} ' +  f"{fluo_measure} {timestamp}" + "\n"
        except Exception as e:
            print(e)

        #### objective monitoring #####
        file = open("/mnt/data/qcontrol3/config/calibrations/automatic/bottom-focus-voltage.csv", "r")
        value = float(file.read())
        output_string += 'objective_focus_piezo_volt{objective="bottom"} ' +  f"{value} {timestamp}" + "\n"
    
    
    time.sleep(6)
    
    ###########  RECON FILE ##############
    try:
        
        ds = xr.load_dataset(get_recon_filename(file_path_short), engine="h5netcdf").dropna("it").isel(i_im=0)
        
        value = ds.phase_1.data[-1]
        output_string += 'reconstruction_results{objective="phase1"} ' +  f"{value} {timestamp}" + "\n"
        
        value = ds.phase_2.data[-1]
        output_string += 'reconstruction_results{objective="phase2"} ' +  f"{value} {timestamp}" + "\n"
        
        value = ds.phase_1_std.data[-1]
        output_string += 'reconstruction_results{objective="phase1std"} ' +  f"{value} {timestamp}" + "\n"
        
        value = ds.phase_2_std.data[-1]
        output_string += 'reconstruction_results{objective="phase2std"} ' +  f"{value} {timestamp}" + "\n"
        
        value = ds.n_atoms_phase.data[-1]
        output_string += 'reconstruction_results{objective="nphaseatoms"} ' +  f"{value} {timestamp}" + "\n"
        
    except Exception as e:
        print(e)
    
    
    return output_string
