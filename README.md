# sequence-monitoring

Program that gathers monitoring from qcontrol3 sequences, evaluates the data and makes the results available as prometheus metrics.


## Prometheus Exporter

First make sure that the prometheus export is active by running ```pipenv run python exporter.py```

## Sequence monitoring

Runs a sequence every couple of minutes, analyzes the image and feeds the results into prometheus.

### Enable monitoring

Run ```./sequence_monitoring <repeat time in seconds>``` inside the pipenv environment.

### Add a new monitoring class

Write a new monitoring class and add it to sequence_checks.py:

```python
class CheckClass:
    config = None
    
    def __init__(self, config):
        self.config = config
    
    async def during_sequence(self):
        # code to be run once it is detected that then shot has started
                
    async def prepare(self):
        #code for preparing tzhe sequence file to be run
        #returns the sequence filename
        
        return filename_out
        
        
    async def evaluate(self, qcontrol_data_path):
        
        #code for evaluating the measurement results and generating prometheus metrics...
        #gets shot folder as parameter
        #returns prometheus metrics as string

        return metrics

```

Add the class name to the config:

```python
checks = dict(
    check_name = dict(
            handler_class = CheckClass,
            do_sth_during_sequence = True, # whether the method "during_sequence" shouldnot be ignored
        ),
    )
```


## Analog Input Monitoring


### Enable monitoring

Run ```pipenv run python start_ai_monitoring.py```. Will evaluate checks for each shot and feed them into prometheus.


### Configure observables

In the config, add entries to the ```analog_input_checks``` dictionary:

analog_input_checks = {
    "mot_fiber_max": {
        "data_path": "/data/ai/pd_mot_fiber/0/data",
        "start_path": "/data/ai/pd_mot_fiber/0/start",
        "freq_path": "/data/ai/pd_mot_fiber/0/frequency",
        "check_function": mean_max,
    },
}

The check_function takes the recorded data and computes an observable. It must be defined in tzhe config as well:


```python
def mean_max(data, time, **kwargs):
    observable = np.mean(data[data > np.max(data) / 2])
    if str(observable) == "nan":
        observable = -1
    return observable
```

