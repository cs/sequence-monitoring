from sequence_checks import BeamMonitoring, TransportMonitoring, BECMonitoring, SuperlatticePhaseMonitoring
import numpy as np

######### SERVER AND FILE SYSTEM SETTINGS ########

server = dict(
        host="172.22.22.173",
        port=8086,
    )

data_root_path = "/mnt/data/measurements/"
ana_base_path = "/mnt/data/analysis"

wait_for_shot_started_timeout = 900
wait_for_shot_finished_timeout = 900

"""checks = dict(
    superlattice1 = dict( 
            handler_class = SuperlatticePhaseMonitoring,
            script_name = "scripts/superlattice1phasecalib.pys",
            do_sth_during_sequence = False,
            axis=1,
            sl_phasecalib_output_file = "/mnt/data/qcontrol3/config/calibrations/automatic/superlattice1_phase.csv",
            metrics_id = "superlattice1_phase"
        ),"""

######## CHECK SEQUENCES SETTINGS ############
checks = dict(
            superlattice2 = dict( 
            handler_class = SuperlatticePhaseMonitoring,
            script_name = "scripts/superlattice2phasecalib.pys",
            do_sth_during_sequence = False,
            axis=0,
            sl_phasecalib_output_file = "/mnt/data/qcontrol3/config/calibrations/automatic/superlattice2_phase.csv",
            metrics_id = "superlattice2_phase"
        ),

    
    #beam_monitoring = dict(  #DEACTIVATED ON 16/01/2023 as we happen not to be using it anymore
    #        handler_class = BeamMonitoring,
    #        do_sth_during_sequence = True,
    #    ),
    #transport_monitoring = dict(
    #        handler_class = TransportMonitoring,
    #        do_sth_during_sequence = False,
    #    ),
    #bec_monitoring = dict(
    #        handler_class = BECMonitoring,
    #        do_sth_during_sequence = False,
    #    ),
)



######## CHECK SEQUENCE SPECIFIC SETTINGS #######


cameras = dict(
    lattice1_in_cam = dict(
            name = "lattice_1_incoming",
            url = "qcontrol3",
            cam_id = "latt1_mako",
            cam_data_folder = "latt1",
            zones=dict(
                    lattice_1_in = dict(startx=0, endx=1900, starty=0, endy=1200,max_sigma=300,min_ampl=1000000),
            ),
        ),
    lattice2_in_cam = dict(
            name = "lattice_2_incoming",
            url = "qcontrol3",
            cam_id = "latt2_mako",
            cam_data_folder = "latt2",
            zones=dict(
                    lattice_2_in = dict(startx=0, endx=1900, starty=0, endy=1200,max_sigma=150,min_ampl=2000000),
            ),
        ),
    lattice1_retro_cam = dict(
            name = "lattice_1_retro_cam",
            url = "http://camera-02.cslab:8000/take/2/20",
            zones=dict(
                    lattice_1_retro = dict(startx=272, endx=2512, starty=22, endy=1800,max_sigma=300,min_ampl=10000000),
            ),
        ),
    #lattice2_retro_cam = dict(
    #        name = "lattice_2_retro_cam",
    #        url = "http://camera-02.cslab:8000/take/3/20",
    #        zones=dict(
    #                lattice_2_retro = dict(startx=272, endx=2512, starty=22, endy=1800,max_sigma=300,min_ampl=10000000),
    #        ),
    #    ),
    
    shvl_and_y = dict(
            name = "shvl_and_y",
            url = "http://camera-02.cslab:8000/take/1/20",
            zones=dict(
                    shvl_top = dict(startx=1033, endx=1951, starty=969, endy=1271,max_sigma=300,min_ampl=0),
                    shvl_bottom = dict(startx=868, endx=1885, starty=1657, endy=1893,max_sigma=300,min_ampl=0),
                    y_dipole = dict(startx=485, endx=1714, starty=1250, endy=1573,max_sigma=300,min_ampl=0),
            ),
        ),
   # z_dipole = dict(
   #         name = "z_dipole",
   #         url = "http://camera-02.cslab:8000/take/0/20",
   #         zones=dict(
   #     
   # 
   #             z_dipole = dict(startx=272, endx=2412, starty=22, endy=1900, max_sigma=1000,min_ampl=300000),
   #         ),
   #     ),
   # 
   )





##############  ANALOG INPUT MONITORING ##################

def mean_max(data, time, **kwargs):
    observable = np.mean(data[data > np.max(data) / 2])
    if str(observable) == "nan":
        observable = -1
    return observable


analog_input_checks = {
    "mot_fiber_max": {
        "data_path": "/data/ai/pd_mot_fiber/0/data",
        "start_path": "/data/ai/pd_mot_fiber/0/start",
        "freq_path": "/data/ai/pd_mot_fiber/0/frequency",
        "check_function": mean_max,
    },
    "raman_lattice_max": {
        "data_path": "/data/ai/pd_raman_lattice/0/data",
        "start_path": "/data/ai/pd_raman_lattice/0/start",
        "freq_path": "/data/ai/pd_raman_lattice/0/frequency",
        "check_function": mean_max,
    },
    "raman_pol_max": {
        "data_path": "/data/ai/pd_raman_polarizer/0/data",
        "start_path": "/data/ai/pd_raman_polarizer/0/start",
        "freq_path": "/data/ai/pd_raman_polarizer/0/frequency",
        "check_function": mean_max,
    },
    "raman_repumper_max": {
        "data_path": "/data/ai/pd_raman_repump/0/data",
        "start_path": "/data/ai/pd_raman_repump/0/start",
        "freq_path": "/data/ai/pd_raman_repump/0/frequency",
        "check_function": mean_max,
    },
    "shvl_max": {
        "data_path": "/data/ai/pd_vert_lattice/0/data",
        "start_path": "/data/ai/pd_vert_lattice/0/start",
        "freq_path": "/data/ai/pd_vert_lattice/0/frequency",
        "check_function": mean_max,
    },
    "steep_vl_max": {
        "data_path": "/data/ai/pd_steep_vert_lattice/0/data",
        "start_path": "/data/ai/pd_steep_vert_lattice/0/start",
        "freq_path": "/data/ai/pd_steep_vert_lattice/0/frequency",
        "check_function": mean_max,
    },
    "767_latt1_max": {
        "data_path": "/data/ai/pd_lattice_767_1/0/data",
        "start_path": "/data/ai/pd_lattice_767_1/0/start",
        "freq_path": "/data/ai/pd_lattice_767_1/0/frequency",
        "check_function": mean_max,
    },
    "767_latt2_max": {
        "data_path": "/data/ai/pd_lattice_767_2/0/data",
        "start_path": "/data/ai/pd_lattice_767_2/0/start",
        "freq_path": "/data/ai/pd_lattice_767_2/0/frequency",
        "check_function": mean_max,
    },
    "1534_latt1_max": {
        "data_path": "/data/ai/pd_lattice_1534_1/0/data",
        "start_path": "/data/ai/pd_lattice_1534_1/0/start",
        "freq_path": "/data/ai/pd_lattice_1534_1/0/frequency",
        "check_function": mean_max,
    },
    "1534_latt2_max": {
        "data_path": "/data/ai/pd_lattice_1534_2/0/data",
        "start_path": "/data/ai/pd_lattice_1534_2/0/start",
        "freq_path": "/data/ai/pd_lattice_1534_2/0/frequency",
        "check_function": mean_max,
    },
    "y_dipole_max": {
        "data_path": "/data/ai/pd_ydipole/0/data",
        "start_path": "/data/ai/pd_ydipole/0/start",
        "freq_path": "/data/ai/pd_ydipole/0/frequency",
        "check_function": mean_max,
    },
    "x_dipole_max": {
        "data_path": "/data/ai/pd_xdipole/0/data",
        "start_path": "/data/ai/pd_xdipole/0/start",
        "freq_path": "/data/ai/pd_xdipole/0/frequency",
        "check_function": mean_max,
    },
    "current_clamp_max": {
        "data_path": "/data/ai/current_clamp/0/data",
        "start_path": "/data/ai/current_clamp/0/start",
        "freq_path": "/data/ai/current_clamp/0/frequency",
        "check_function": mean_max,
    },
    "molasses_max": {
        "data_path": "/data/ai/pd_molasses/0/data",
        "start_path": "/data/ai/pd_molasses/0/start",
        "freq_path": "/data/ai/pd_molasses/0/frequency",
        "check_function": mean_max,
    },
    "gauss_max": {
        "data_path": "/data/ai/pd_gauss/0/data",
        "start_path": "/data/ai/pd_gauss/0/start",
        "freq_path": "/data/ai/pd_gauss/0/frequency",
        "check_function": mean_max,
    },
    "current_x1_max": {
        "data_path": "/data/ai/current_x1/0/data",
        "start_path": "/data/ai/current_x1/0/start",
        "freq_path": "/data/ai/current_x1/0/frequency",
        "check_function": mean_max,
    },
    "current_x2_max": {
        "data_path": "/data/ai/current_x2/0/data",
        "start_path": "/data/ai/current_x2/0/start",
        "freq_path": "/data/ai/current_x2/0/frequency",
        "check_function": mean_max,
    },
    "current_zg1_max": {
        "data_path": "/data/ai/current_zg1/0/data",
        "start_path": "/data/ai/current_zg1/0/start",
        "freq_path": "/data/ai/current_zg1/0/frequency",
        "check_function": mean_max,
    },
    "current_zg2_max": {
        "data_path": "/data/ai/current_zg2/0/data",
        "start_path": "/data/ai/current_zg2/0/start",
        "freq_path": "/data/ai/current_zg2/0/frequency",
        "check_function": mean_max,
    },
    "gc_repumper_max": {
        "data_path": "/data/ai/pd_gc_repumper/0/data",
        "start_path": "/data/ai/pd_gc_repumper/0/start",
        "freq_path": "/data/ai/pd_gc_repumper/0/frequency",
        "check_function": mean_max,
    },
    "pd_molasses_diag_retro_max": {
        "data_path": "/data/ai/pd_molasses_diag_retro/0/data",
        "start_path": "/data/ai/pd_molasses_diag_retro/0/start",
        "freq_path": "/data/ai/pd_molasses_diag_retro/0/frequency",
        "check_function": mean_max,
    },
    "pd_molasses_hor_retro_max": {
        "data_path": "/data/ai/pd_molasses_hor_retro/0/data",
        "start_path": "/data/ai/pd_molasses_hor_retro/0/start",
        "freq_path": "/data/ai/pd_molasses_hor_retro/0/frequency",
        "check_function": mean_max,
    },
}
